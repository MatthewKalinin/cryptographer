﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Kursach2.Models;
using Microsoft.AspNetCore.Http;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

namespace Kursach2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Encrypt()
        {
            return View();
        }
        public IActionResult Decrypt()
        {
            return View();
        }
        
        
        [HttpGet]
        public FileResult GetTxtFileResult(string message)
        {
            System.IO.File.WriteAllText("wwwroot/Files/out.txt", message);
            byte[] buff = System.IO.File.ReadAllBytes("wwwroot/Files/out.txt");
            System.IO.File.Delete("wwwroot/Files/out.txt");
            return File(buff, "text/plain", "Result.txt");
        }
        [HttpGet]
        public FileResult GetDocxFileResult(string message)
        {
            string path = "wwwroot/Files/out.docx";
            string[] messageArr = message.Split('\n');
            using (var doc = WordprocessingDocument.Create(path, DocumentFormat.OpenXml.WordprocessingDocumentType.Document))
            {
                MainDocumentPart mainDocument = doc.AddMainDocumentPart();
                mainDocument.Document = new Document();
                Body body = mainDocument.Document.AppendChild(new Body());
                foreach (var par in messageArr)
                {
                    var para = body.AppendChild(new Paragraph());
                    var run = para.AppendChild(new Run());
                    run.AppendChild(new Text(par));
                }
            }
            byte[] buff = System.IO.File.ReadAllBytes(path);
            System.IO.File.Delete(path);
            return File(buff, "application/octet-stream", "Result.docx");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


    }
}
