﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Kursach2.Models;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace Kursach2.Controllers
{
    public class DecryptController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddMessage(string key, string message, IFormFile uploadFile)
        {
            if (uploadFile != null && message != null)
            {
                return View("ErrorResult", "Отправлено сразу два поля");
            }
            if (uploadFile == null && message == null)
            {
                return View("ErrorResult", "Отправленные поля оказались пустыми");
            }

            MessageModel model = new MessageModel();
            if (uploadFile != null)
            {
                string type = uploadFile.FileName.Split('.')[1];
                if (!(type != "txt" || type != "docx"))
                {
                    return View("ErrorResult", "Отправлен неверный формат файла");
                }
                string path = "wwwroot/Files/" + uploadFile.FileName;
                using (var filestream = new FileStream(path, FileMode.OpenOrCreate))
                {
                    await uploadFile.CopyToAsync(filestream);
                }
                model.Key = key;
                model.PathFile = path;
                model.Parse(type);
            }
            else if (message != null)
            {
                model.Key = key;
                model.FirstMessage = message;
            }
            model.Decrypt();
            return View("Result", model);
        }
    }
}
