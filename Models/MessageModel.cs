﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Bibliography;
using System.Text;

namespace Kursach2.Models
{
    public class MessageModel
    {
        public string FirstMessage { get; set; }
        public string ResultMessage { get; set; }
        public string Key { get; set; }
        public string PathFile { get; set; }

        public void Encrypt()
        {
            string alphUpper = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
            string alphLower = alphUpper.ToLower();
            string keyMessage = "";
            int j = 0;
            for (int i = 0; i < FirstMessage.Length; i++)
            {
                if (alphLower.Contains(FirstMessage[i]) || alphUpper.Contains(FirstMessage[i]))
                {
                    keyMessage += Key[j];
                    if (++j == Key.Length)
                        j = 0;
                }
                else
                {
                    keyMessage += FirstMessage[i];
                }
            }

            string encryptStr = "";

            for (int i = 0; i < FirstMessage.Length; i++)
            {
                char letter = FirstMessage[i];
                char keyLetter = keyMessage[i];
                char newLetter;
                if (alphUpper.Contains(letter))
                {
                    int id = alphUpper.IndexOf(letter);
                    int step = alphLower.IndexOf(keyLetter);
                    id += step;

                    if (id < 0)
                        id += 33;
                    else if (id >= 33)
                        id -= 33;

                    newLetter = alphUpper[id];
                    encryptStr += newLetter;
                }
                else if (alphLower.Contains(letter))
                {
                    int id = alphLower.IndexOf(letter);
                    int step = alphLower.IndexOf(keyLetter);
                    id += step;

                    if (id < 0)
                        id += 33;
                    else if (id >= 33)
                        id -= 33;

                    newLetter = alphLower[id];
                    encryptStr += newLetter;
                }
                else
                {
                    newLetter = FirstMessage[i];
                    encryptStr += newLetter;
                }
            }

            ResultMessage = encryptStr;
        }
        public void Decrypt()
        {
            string alphUpper = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
            string alphLower = alphUpper.ToLower();

            string keyMessage = "";
            int j = 0;
            for (int i = 0; i < FirstMessage.Length; i++)
            {
                if (alphLower.Contains(FirstMessage[i]) || alphUpper.Contains(FirstMessage[i]))
                {
                    keyMessage += Key[j];
                    if (++j == Key.Length)
                        j = 0;
                }
                else
                {
                    keyMessage += FirstMessage[i];
                }
            }

            string decryptStr = "";

            for (int i = 0; i < FirstMessage.Length; i++)
            {
                char letter = FirstMessage[i];
                char keyLetter = keyMessage[i];
                char newLetter;
                if (alphUpper.Contains(letter))
                {
                    int id = alphUpper.IndexOf(letter);
                    int step = alphLower.IndexOf(keyLetter);
                    id -= step;

                    if (id < 0)
                        id += 33;
                    else if (id >= 33)
                        id -= 33;

                    newLetter = alphUpper[id];
                    decryptStr += newLetter;
                }
                else if (alphLower.Contains(letter))
                {
                    int id = alphLower.IndexOf(letter);
                    int step = alphLower.IndexOf(keyLetter);
                    id -= step;

                    if (id < 0)
                        id += 33;
                    else if (id >= 33)
                        id -= 33;

                    newLetter = alphLower[id];
                    decryptStr += newLetter;
                }
                else
                {
                    newLetter = FirstMessage[i];
                    decryptStr += newLetter;
                }
            }

            ResultMessage = decryptStr;
        }
        public void Parse(string type)
        {
            if (type == "txt")
            {
                /*как оказалось при чтении данных из txt файла стоит учитывать кодировку
                 * здесь будем пробовать подбирать нужную нам кодировку
                 * чтобы особо не заморачиваться будем использовать самые частые:
                 * ANSI (потому что выданный файлик в этой кодировке)
                 * ASCII, Unicode, UTF-8*/


                string alph = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
                alph += "ABCDEFGHIJKLMNOPRSTUVWXYZ";
                alph += alph.ToLower();

                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

                //пробуем ANSI
                FirstMessage = File.ReadAllText(PathFile, Encoding.GetEncoding(1251));
                if (FirstMessage.All(t => alph.Contains(t) || Char.IsWhiteSpace(t) || Char.IsNumber(t) || Char.IsPunctuation(t)))
                    return;
                //пробуем ASCII
                FirstMessage = File.ReadAllText(PathFile, Encoding.ASCII);
                if (FirstMessage.All(t => alph.Contains(t) || Char.IsWhiteSpace(t) || Char.IsNumber(t) || Char.IsPunctuation(t)))
                    return;
                //пробуем Unicode
                FirstMessage = File.ReadAllText(PathFile, Encoding.Unicode);
                if (FirstMessage.All(t => alph.Contains(t) || Char.IsWhiteSpace(t) || Char.IsNumber(t) || Char.IsPunctuation(t)))
                    return;
                //пробуем UTF-8
                FirstMessage = File.ReadAllText(PathFile, Encoding.UTF8);
                if (FirstMessage.All(t => alph.Contains(t) || Char.IsWhiteSpace(t) || Char.IsNumber(t) || Char.IsPunctuation(t)))
                    return;

                //если ни одна кодировка не подошла возвращаем текст с дефолтной кодировкой
                FirstMessage = File.ReadAllText(PathFile);
            }
            else
            {
                string text = "";
                using (var document = WordprocessingDocument.Open(PathFile, false))
                {
                    var elements = document.MainDocumentPart.Document.Body.Elements();
                    foreach (var selection in elements)
                    {
                        if (selection.LocalName == "p")
                            text += selection.InnerText + "\n";
                    }
                }
                FirstMessage = text;
            }
            File.Delete(PathFile);
            return;
        }
    }
}
